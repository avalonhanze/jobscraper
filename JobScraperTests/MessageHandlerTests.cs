﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobScraper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace JobScraper.Tests
{
    [TestClass()]
    public class MessageHandlerTests
    {
        [TestMethod()]
        public void HandleMessageTest()
        {
            MessageHandler mh = new MessageHandler();
            String test = "test";
            String result = mh.HandleMessage(test);
            Assert.IsNotNull(result, "MessageHandler returns null");
            Assert.IsInstanceOfType(result, typeof(String), "Messaghandler returns not a String");
            Assert.AreNotSame(test, result, "Returning message is same as received");
            Assert.AreNotEqual(test, result, "Returning message is equal to received");
        }
    }
}
