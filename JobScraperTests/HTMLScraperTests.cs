﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobScraper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
namespace JobScraper.Tests
{
    [TestClass()]
    public class HTMLScraperTests
    {
        [TestMethod()]
        public void ScrapeSiteTest()
        {
            String uri = "www.nu.nl";
            HTMLScraper hs = new HTMLScraper();
            XDocument doc = hs.ScrapeSite(uri);
            Assert.IsNotNull(doc, "Scrapesite returns null");
            Assert.IsInstanceOfType(doc, typeof(XDocument), "Scrapesite returns not a XDocument");
        }
    }
}
