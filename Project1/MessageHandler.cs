﻿using System;
using System.Diagnostics;
using System.Net;
using System.Xml;
using System.Xml.Linq;


namespace JobScraper
{
    /// <summary>
    /// Class which functions as the middleman between the client and scraper
    /// </summary>
    public class MessageHandler
    {
        HTMLScraper hs = new HTMLScraper();
        /// <summary>
        /// Checks the message for problems, and converts it into a Xmlstring result
        /// </summary>
        /// <param name="body">The message from the client</param>
        /// <returns>the XMLstring result</returns>
        public String HandleMessage(string body)
        {
            try
            {
                String searchString = ParseBody(body);
                if (String.IsNullOrEmpty(searchString))
                {
                    return "<errormessage>Message not processed, no company was found in the body to search for</errormessage>";
                }
                XDocument result = GetResult(searchString);
                String newBody = ParseResult(result);
                return newBody;
            }
            catch (XmlException ex)
            {
                return "<errormessage>" + ex.ToString() + "</errormessage>";
            }

        }
        /// <summary>
        /// Retrieves the searchstring from the XMLstring of the message body 
        /// </summary>
        /// <param name="body">The body of the client message</param>
        /// <returns>The string of the company to be searched for</returns>
        private string ParseBody(string body)
        {
            XDocument xbody = XDocument.Parse(body);
            string searchString = xbody.Element("search").Element("string").Value;
            return searchString;
        }
        /// <summary>
        /// Retrieves the XDocument search result of the company from the HTMLscraper
        /// </summary>
        /// <param name="searchString">the company name to be searched for</param>
        /// <returns>The XDocument XML result of the search</returns>
        private XDocument GetResult(string searchString)
        {
            String uri = "http://www.indeed.nl/vacatures?q=" + WebUtility.UrlEncode(searchString);
            Debug.WriteLine(uri);
            XDocument result = hs.ScrapeSite(uri);
            return result;
        }
        /// <summary>
        /// Parses the XDocument into a XML string
        /// </summary>
        /// <param name="result">The searchresult</param>
        /// <returns>The result in XML string format</returns>
        private String ParseResult(XDocument result)
        {
            return result.ToString();
        }
    }
}
