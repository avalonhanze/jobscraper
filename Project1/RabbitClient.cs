﻿using JobScraper.Properties;
using RabbitMQ.Client;
using System;
using System.Diagnostics;

namespace JobScraper
{
    /// <summary>
    /// Class connecting to the Rabbit MQ message broker, receiving and sending messages.
    /// </summary>
    class RabbitClient
    {
        ConnectionFactory cf = new ConnectionFactory();
        MessageHandler mh = new MessageHandler();
        IConnection conn;
        QueueingBasicConsumer consumer;
        IModel rx;
        IModel tx;
        /// <summary>
        /// Constructor for the RabbitClient
        /// </summary>
        public RabbitClient()
        {

            cf.UserName = Settings.Default.UserName;
            cf.Password = Settings.Default.Password;
            cf.HostName = Settings.Default.HostName;
            cf.Port = Settings.Default.Port;
            cf.VirtualHost = Settings.Default.VirtualHost;
            try
            {
                conn = cf.CreateConnection();
                rx = conn.CreateModel();
                consumer = new QueueingBasicConsumer(rx);
                String consumerTag = rx.BasicConsume(Settings.Default.Requestqueue, false, consumer);
                tx = conn.CreateModel();
                tx.QueueDeclare(Settings.Default.Resultqueue, true, false, false, null);
            }
            catch (Exception ex)
            {

                Debug.WriteLine("Exception source: {0}", ex.ToString());

            }

        }
        /// <summary>
        /// Creates the listener for the message broker
        /// </summary>
        public void CreateListener()
        {
            while (true)
            {
                try
                {
                    RabbitMQ.Client.Events.BasicDeliverEventArgs e =
                    (RabbitMQ.Client.Events.BasicDeliverEventArgs)
                    consumer.Queue.Dequeue();
                    CheckMessage(e);
                }
                catch (Exception ex)
                {
                    // The consumer was removed, either through
                    // channel or connection closure, or through the
                    // action of IModel.BasicCancel().
                    SendMessage("Exception source: {0}", "<errormessage>" + ex.ToString() + "</errormessage>");
                    Debug.WriteLine("Exception source: {0, }", ex.ToString());
                    break;
                }
            }


        }
        /// <summary>
        /// Checks if the message has a correlation ID and a body
        /// </summary>
        /// <param name="e">The message from the broker</param>
        private void CheckMessage(RabbitMQ.Client.Events.BasicDeliverEventArgs e)
        {
            IBasicProperties props = e.BasicProperties;
            Debug.WriteLine(props.CorrelationId);
            String corrID = props.CorrelationId;
            String body = System.Text.Encoding.Default.GetString(e.Body);
            if (String.IsNullOrEmpty(corrID))
            {
                SendMessage("No ID", "<errormessage>Message not processed, no correlation ID was found</errormessage>");
            }
            else if (String.IsNullOrEmpty(body))
            {
                SendMessage("corrID", "<errormessage>Message not processed, no XML data found in the body</errormessage>");
            }
            else
            {
                String newBody = mh.HandleMessage(body);
                SendMessage(corrID, newBody);
                Debug.WriteLine(newBody);
                Debug.WriteLine(body);

            }
            rx.BasicAck(e.DeliveryTag, false);
        }
        /// <summary>
        /// Sends a given message to the server
        /// </summary>
        /// <param name="corrID">The correlation ID of the received message</param>
        /// <param name="newBody">The search result or error message</param>
        private void SendMessage(string corrID, string newBody)
        {
            try
            {
                byte[] messageBody = System.Text.Encoding.UTF8.GetBytes(newBody);
                IBasicProperties newProps = tx.CreateBasicProperties();
                newProps.CorrelationId = corrID;
                tx.BasicPublish("", Settings.Default.Resultqueue, newProps, messageBody);
            }
            catch (Exception ex)
            {
                // The consumer was removed, either through
                // channel or connection closure, or through the
                // action of IModel.BasicCancel()
                Debug.WriteLine("Exception source: {0, }", ex.ToString());
            }

        }




    }
}
