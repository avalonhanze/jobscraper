﻿using HtmlAgilityPack;
using System;
using System.Diagnostics;
using System.Xml.Linq;
using System.Text.RegularExpressions;


namespace JobScraper
{
    /// <summary>
    /// Class for scraping the HTML of a given URI for searchResultItems
    /// </summary>
    public class HTMLScraper
    {
        /// <summary>
        /// Scrape the html of the given URI
        /// </summary>
        /// <param name="uri">The URI to be scraped</param> 
        /// <returns>An XDocument with the scraped information in XML </returns>
        public XDocument ScrapeSite(String uri)
        {
            HtmlWeb hw = new HtmlWeb();
            XDocument scrapeResult = new XDocument();
            XElement jobs = new XElement("jobs");
            scrapeResult.Add(jobs);
            try
            {
                HtmlDocument doc = hw.Load(@uri);
                HtmlNodeCollection coll = doc.DocumentNode.SelectNodes("//h2[@class='jobtitle'] //a[@href]");
                if (coll != null)
                {
                    XElement result = getResultElement(coll, jobs);
                }

            }
            catch (UriFormatException e)
            {
                if (e.Source != null)
                    Debug.WriteLine("IOException source: {0}", e.Source);
            }
            return scrapeResult;
        }
        /// <summary>
        /// Extracts the information from the collection of nodes, and puts them in XElements.
        /// </summary>
        /// <param name="coll">collection of HTML nodes</param>
        /// <param name="jobs">the XElement where the Xelements are collected in</param>
        /// <returns>The filled Xelement</returns>
        private XElement getResultElement(HtmlNodeCollection coll, XElement jobs)
        {

            foreach (HtmlNode link in coll)
            {
                XElement result = new XElement("jobitem");
                HtmlAttribute att = link.Attributes["href"];

                result.Add(
                    new XElement("uri", "http://www.indeed.nl" + att.Value.Trim()),
                    new XElement("title", Regex.Replace(link.InnerText.Trim(), @"\t|\n|\r", "")));
                jobs.Add(result);
            }
            return jobs;
        }
    }
}
